// Toggle searchBar animation
$('#searchToggle').click(function () {
    $(this).toggleClass('active');
    $('#searchOverlay').toggleClass('open');
    $('#searchToggle').toggleClass('search-icon');

    $('#closeSearch').click(function () {
        $('#searchOverlay').removeClass('open');
        $('#searchToggle').removeClass('search-icon');
    });
});


/** Add pre-loader before page load **/
jQuery(document).ready(function () {
    $('#preloader').fadeOut('10ms', function () {
        $('#preloader').remove();
    });
});


//add slick slideshow
$('.board-members').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: true,
    autoplay: false
});




// hide sidebar if inactive

var timedelay = 1;
function delayCheck() {
    if (timedelay == 5) {
        $('#sidebarNavigation').fadeOut();
        timedelay = 1;
    }
    timedelay = timedelay + 1;
}

$(document).mousemove(function () {
    $('#sidebarNavigation').fadeIn();
    timedelay = 1;
    clearInterval(_delay);
    _delay = setInterval(delayCheck, 500);
});
// page loads starts delay timer
_delay = setInterval(delayCheck, 500);


/** Jquery Count up numbers when scrolled into view
 * Add class 'jquery-countup' to heading or paragraph tag with a number to count up to
 * ie <h1 class="jquery-countup">100</h1>
 * **/

function countUp(element) {
    //duration of count = 2000ms
    var time = 1;
    var num = element.text();
    num = parseInt(num.replace(/ /g, ''));
    //set this number to 0
    element
        .text(0);
    //figure out what to count up in
    var inc = parseInt(num) / (time * 40);
    var current = 0
    var timer = setInterval(function () {
        current += inc;
        element
            .text(format("#,###.##", parseInt(current)));
        if (current >= num) {
            clearInterval(timer);
        }
    }, 25);
}
var hasScrolled = false;
function isScrolledIntoView(elem) {
    if (typeof (elem) !== 'undefined') {
        var docViewTop = jQuery(window).scrollTop();
        var docViewBottom = docViewTop + jQuery(window).height();
        var elemTop = jQuery(elem).offset().top;
        var elemBottom = elemTop + jQuery(elem).height();
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
}
jQuery(window).scroll(function () {
    if ($('.jquery-countup').length) {
        if (isScrolledIntoView('.jquery-countup') && hasScrolled == false) {
            hasScrolled = true; //only do this once
            jQuery('.jquery-countup').each(function () {
                countUp(jQuery(this));
            });
        }
    }
});

/**
 IntegraXor Web SCADA - JavaScript Number Formatter
 http://www.integraxor.com/
 author: KPL, KHL
 (c)2011 ecava
 Dual licensed under the MIT or GPL Version 2 licenses.
 **/
window.format = function (b, a) {
    if (!b || isNaN(+a)) return a;
    var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split(".");
    if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1);
    d = b[0].split(e);
    b[0] = d.join("");
    var f = b[0] && b[0].indexOf("0");
    if (f > -1) for (; c[0].length < b[0].length - f;)c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = "");
    a = a.split(".");
    a[0] = c[0];
    if (c = d[1] && d[d.length -
        1].length) {
        for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++)f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e);
        a[0] = f
    }
    a[1] = b[1] && a[1] ? h + a[1] : "";
    return (j ? "-" : "") + a[0] + a[1]
};

// add pulse effect on all link clicks
//var links = document.querySelectorAll('a');
//for (var i = 0, len = links.length; i < len; i++) {
//    links[i].addEventListener('click', function (e) {
//        var targetEl = e.target;
//        var inkEl = targetEl.querySelector('.ink');
//
//        if (inkEl) {
//            inkEl.classList.remove('animate');
//        }
//        else {
//            inkEl = document.createElement('span');
//            inkEl.classList.add('ink');
//            inkEl.style.width = inkEl.style.height = Math.max(targetEl.offsetWidth, targetEl.offsetHeight) + 'px';
//            targetEl.appendChild(inkEl);
//        }
//
//        inkEl.style.left = (e.offsetX - inkEl.offsetWidth / 2) + 'px';
//        inkEl.style.top = (e.offsetY - inkEl.offsetHeight / 2) + 'px';
//        inkEl.classList.add('animate');
//    }, false);
//}


//firstrand foundation section
$('#text1show').mouseover(function () {
    $(".foundation-learning-text").fadeIn();
    $(".foundation-future-text").fadeOut();
    $(".foundation-economy-text").fadeOut();

    $(".foundation-models").fadeOut(0);
    $("#text3shows").fadeIn();
});
$('#text2show').mouseover(function () {
    $(".foundation-learning-text").fadeOut();
    $(".foundation-future-text").fadeOut();
    $(".foundation-economy-text").fadeIn();

    $(".foundation-models").fadeOut(0);
    $("#text1shows").fadeIn();
});
$('#text3show').mouseover(function () {
    $(".foundation-learning-text").fadeOut();
    $(".foundation-future-text").fadeIn();
    $(".foundation-economy-text").fadeOut();

    $(".foundation-models").fadeOut(0);
    $("#text2shows").fadeIn();
});

//toggle country blocks in regional map
jQuery(document).ready(function () {
    $.fn.showCountryBlocks = function (countryId) {
        $(this).hover(function () {
            var country = this;
            $(countryId).toggleClass('show');
            $(country).toggleClass('pulse');

            $(countryId).hover(function () {
                $(countryId).addClass('show');
                $(country).addClass('fill');
                $(countryId).mouseleave(function () {
                    $(countryId).removeClass('show');
                    $(country).removeClass('pulse');
                });
            });
            return this;

        });
    };
});

jQuery(document).ready(function () {
    //countries
    $("#southAfrica").showCountryBlocks("#toggleSouthAfrica");
    $("#botswana").showCountryBlocks("#toggleBotswana");
    $("#namibia").showCountryBlocks("#toggleNamibia");
    $("#zambia").showCountryBlocks("#toggleZambia");
    $("#mozambique").showCountryBlocks("#toggleMozambique");
    $("#tanzania").showCountryBlocks("#toggleTanzania");
    $("#kenya").showCountryBlocks("#toggleKenya");
    $("#angola").showCountryBlocks("#toggleAngola");
    $("#ghana").showCountryBlocks("#toggleGhana");
    $("#nigeria").showCountryBlocks("#toggleNigeria");
    $("#india").showCountryBlocks("#toggleIndia");
    $("#uk").showCountryBlocks("#toggleUK");
    $("#china").showCountryBlocks("#toggleChina");
    $("#guernsey").showCountryBlocks("#toggleGuernsey");
    //countries
    $("#southAfrica").showCountryBlocks("#toggleSouthAfrica");
    $("#botswana").showCountryBlocks("#toggleBotswana");
    $("#namibia").showCountryBlocks("#toggleNamibia");
    $("#zambia").showCountryBlocks("#toggleZambia");
    $("#mozambique").showCountryBlocks("#toggleMozambique");
    $("#tanzania").showCountryBlocks("#toggleTanzania");
    $("#kenya").showCountryBlocks("#toggleKenya");
    $("#angola").showCountryBlocks("#toggleAngola");
    $("#ghana").showCountryBlocks("#toggleGhana");
    $("#nigeria").showCountryBlocks("#toggleNigeria");
    $("#india").showCountryBlocks("#toggleIndia");
    $("#uk").showCountryBlocks("#toggleUK");
    $("#china").showCountryBlocks("#toggleChina");
    //$("#guernsey").showCountryBlocks("#toggleGuernsey");

    //circles
    $("#southAfricaCircle").showCountryBlocks("#toggleSouthAfrica");
    $("#botswanaCircle").showCountryBlocks("#toggleBotswana");
    $("#namibiaCircle").showCountryBlocks("#toggleNamibia");
    $("#zambiaCircle").showCountryBlocks("#toggleZambia");
    $("#mozambiqueCircle").showCountryBlocks("#toggleMozambique");
    $("#tanzaniaCircle").showCountryBlocks("#toggleTanzania");
    $("#kenyaCircle").showCountryBlocks("#toggleKenya");
    $("#angolaCircle").showCountryBlocks("#toggleAngola");
    $("#ghanaCircle").showCountryBlocks("#toggleGhana");
    $("#nigeriaCircle").showCountryBlocks("#toggleNigeria");
    $("#indiaCircle").showCountryBlocks("#toggleIndia");
    $("#ukCircle").showCountryBlocks("#toggleUK");
    $("#chinaCircle").showCountryBlocks("#toggleChina");
    //$("#guernseyCircle").showCountryBlocks("#toggleGuernsey");
});

// toggle accordion on click
$('#investor-presentations').click(function (e) {
    $('#collapseOne').collapse('show');
});

$('#share-price').click(function (e) {
    $('#collapseTwo').collapse('show');
});

$('#other-shareholders').click(function (e) {
    $('#collapseThree').collapse('show');
});

$('#basel-3-pillar').click(function (e) {
    $('#collapseFour').collapse('show');
});


// regional presence info

$('.uk').click(function () {
    $(".country-info").fadeOut();
    $("#toggleUK").toggle();
});
$('.namibia').click(function () {
    $(".country-info").fadeOut();
    $("#toggleNamibia").toggle();
});
$('.southAfrica').click(function () {
    $(".country-info").fadeOut();
    $("#toggleSouthAfrica").toggle();
});
$('.mozambique').click(function () {
    $(".country-info").fadeOut();
    $("#toggleMozambique").toggle();
});
$('.zambia').click(function () {
    $(".country-info").fadeOut();
    $("#toggleZambia").toggle();
});
$('.tanzania').click(function () {
    $(".country-info").fadeOut();
    $("#toggleTanzania").toggle();
});
$('.ghana').click(function () {
    $(".country-info").fadeOut();
    $("#toggleGhana").toggle();
});
$('.nigeria').click(function () {
    $(".country-info").fadeOut();
    $("#toggleNigeria").toggle();
});
$('.botswana').click(function () {
    $(".country-info").fadeOut();
    $("#toggleBotswana").toggle();
});
$('.india').click(function () {
    $(".country-info").fadeOut();
    $("#toggleIndia").toggle();
});
$('.china').click(function () {
    $(".country-info").fadeOut();
    $("#toggleChina").toggle();
});
$('.angola').click(function () {
    $(".country-info").fadeOut();
    $("#toggleAngola").toggle();
});
$('.kenya').click(function () {
    $(".country-info").fadeOut();
    $("#toggleKenya").toggle();
});


// End map

// toggle accordion on click
$('#investor-presentations').click(function (e) {
    $('#collapseOne').collapse('show');
});

$('#share-price').click(function (e) {
    $('#collapseTwo').collapse('show');
});

$('#other-shareholders').click(function (e) {
    $('#collapseThree').collapse('show');
});

$('#basel-3-pillar').click(function (e) {
    $('#collapseFour').collapse('show');
});


// Text to speech for perspectives articles
onload = function () {
    if ('speechSynthesis' in window) with (speechSynthesis) {

        var playEle = document.querySelector('#play');
        var pauseEle = document.querySelector('#pause');
        var stopEle = document.querySelector('#stop');
        var flag = false;

        if (playEle) {
            playEle.addEventListener('click', onClickPlay);
        }
        if (pauseEle) {
            pauseEle.addEventListener('click', onClickPause);
        }
        if (stopEle) {
            stopEle.addEventListener('click', onClickStop);
        }

        
       

        function onClickPlay() {
            if (!flag) {
                flag = true;
                utterance = new SpeechSynthesisUtterance(document.querySelector('.article').textContent);
                utterance.voice = getVoices()[0];
                utterance.onend = function () {
                    flag = false;
                    playEle.className = pauseEle.className = '';
                    stopEle.className = 'stopped';
                };
                playEle.className = 'played';
                pauseEle.className = '';
                stopEle.className = '';
                speak(utterance);
            }
            if (paused) { /* unpause/resume narration */
                playEle.className = 'played';
                pauseEle.className = '';
                resume();
            }
        }

        function onClickPause() {
            if (speaking && !paused) { /* pause narration */
                pauseEle.className = 'paused';
                playEle.className = '';
                pause();
            }
        }

        function onClickStop() {
            if (speaking) { /* stop narration */
                /* for safari */
                stopEle.className = 'stopped';
                playEle.className = pauseEle.className = '';
                flag = false;
                cancel();

            }
        }
    }

    else { /* speech synthesis not supported */
        msg = document.createElement('h5');
        msg.textContent = "Detected no support for Speech Synthesis";
        msg.style.textAlign = 'center';
        msg.style.backgroundColor = 'red';
        msg.style.color = 'white';
        msg.style.marginTop = msg.style.marginBottom = 0;
        document.body.insertBefore(msg, document.querySelector('div'));
    }
};

// Perspectives related articles
// Create carousel with horizontal scroll effect similar to netflix
$.fn.make_carousel = function () {
    $(".nav-btn").click(function () {
        var box = $("#carousel1"),
            x;
        if ($(this).hasClass("next")) {
            x = ((box.width() / 1)) + box.scrollLeft();
            box.animate({
                scrollLeft: x,
            })
        } else {
            x = ((box.width() / 1)) - box.scrollLeft();
            box.animate({
                scrollLeft: -x,
            })
        }
    })
};
$("#carousel1").make_carousel();
$("#carousel2").make_carousel();


//modal gallery lightbox




// // Active class to menu items
// $(function () {
//    var current = location.pathname;
//    $('ul#sidenav li a').each(function () {
//        var $this = $(this);
//        // if the current path is like this link, make it active
//        if ($this.prop('href').indexOf(current) !== -1) {
//            $this.addClass('active');
//        }
//    });
// 
//    // Remove active class from sublist under investor information
//    var x = $(".sidebar-sticky ul li ul").find("a");
//     if (x.hasClass("active") === true) {
//         x.removeClass("active");
//     }
// });
//
//$(function () {
//    var current = location.pathname;
//    $('.navbar-nav li a').each(function () {
//        var $this = $(this);
//        // if the current path is like this link, make it active
//        if ($this.prop('href').indexOf(current) !== -1) {
//            $this.addClass('active');
//        }
//    });
//
//    // Remove active class from sublist under investor information
//    //var x = $(".navbar-nav li a").find("a");
//    //if (x.hasClass("active") === true) {
//    //    x.removeClass("active");
//    //}
//});

//$(function () {
//    var current = location.pathname;
//    $('ul#sidenav li a').each(function () {
//        var $this = $(this);
//        // if the current path is like this link, make it active
//        if ($this.attr('href').indexOf(current) !== -1) {
//            $this.addClass('active');
//        }
//    })
//});
//
//$(function () {
//    var current = location.pathname;
//    $('.navbar-nav li a').each(function () {
//        var $this = $(this);
//        // if the current path is like this link, make it active
//        if ($this.prop('href').indexOf(current) !== -1) {
//            $this.addClass('active');
//        }
//    })
//});

// start slick slider
$('.related-items-slideshow').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 3
});


function openNav(evt) {

    if (evt.target.id == "report-archive") {
        document.getElementById("vertical-menu__full__site__map").style.width = "95%";
        document.getElementById("vertical-menu__full__site__map").style.height = "100%";


    }
    else if (evt.target.id == "full-site-map") {
        document.getElementById("vertical-menu__report__archive").style.width = "95%";
        document.getElementById("vertical-menu__report__archive").style.height = "100%";
        console.log("full site map");


    }

}

function closeNav(evt) {
    if (evt.target.id == "close-me__1") {
        document.getElementById("vertical-menu__full__site__map").style.width = "0";


    }
    else if (evt.target.id == "close-me__2") {
        document.getElementById("vertical-menu__report__archive").style.width = "0";
    }


}

$(document).on('click', '.dropdown-menu .nav-item a', function () {
    $(".dropdown-menu .nav-item a").removeClass("active");
    $(this).addClass("active");
});


$(document).ready(function () {
    
    var current = location.pathname;

    
   var pages = ['/society/foundations/firstrand-foundation/', '/society/foundations/foundations/', '/society/foundations/firstrand-empowerment-fund/', '/society/foundations/firstrand-empowerment-fund/','/society/foundations/staff-assistance-trust/', '/society/foundations/knowledge-sharing/'];
   
   var i;
   
   for (i = 0; i < pages.length; i++) {
        
        if (pages[i] === current) {
            $('.addActive').addClass('active');     
            $('.sub-list').css('display', 'block');
           
           var currentPage = $('.sub-list').find('li a');
         
           
           if (currentPage.prop('href').indexOf(current) !== -1) {
               
           }
        }
    }
   
    $('ul#sidenav li a').on('click', function () {
        //remove class
        var x = $(".sidebar-sticky ul li").find("a");
        if (x.hasClass("active") === true) {
            x.removeClass("active");
        }

        if ($(this).hasClass('addActive')) {
            $(this).addClass('active');
        }
    });

});

   /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
        });
    }




//function closeDisclaimer() {
//    document.getElementById("disclaimer").style.width = "0%";
//}

$(window).on('load', function () {
    $('#myNav').modal('show');

  
});

//function openNav() {
//    document.getElementById("myNav").style.width = "100%";
//}

//function closeNav() {
//    document.getElementById("myNav").style.width = "0%";
//    }


$(document).ready(function () {
    $('#myDis').fadeIn();
});
$('button').click(function () {
    $('#myDis').fadeOut(200, "linear");
});
function openDis() {
    document.getElementById("myDis").style.height = "100%";
}

function closeDis() {
    document.getElementById("myDis").style.height = "0%";
}
